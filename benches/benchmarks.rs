#[macro_use]
extern crate criterion;

use criterion::Criterion;

use arcos_kdl::geometry::{EulerBuild, Frame, Twist};
use arcos_kdl::joint::JointType;
use arcos_kdl::kinematic_arm::KinematicArm;
use arcos_kdl::kinematic_arm::SegmentDescription as Desc;
use rand::prelude::*;
use std::f64::consts::PI;

fn create_kuka_description() -> Vec<Desc> {
    vec![
        Desc::shoulder(Frame::from_translation_euler(0.0, 0.0, 0.0, 0.0, 0.0, 0.0)),
        Desc {
            // segment 1
            joint_type: JointType::RotZ,
            joint_tf: Frame::from_translation_euler(0.0, 0.0, 0.2, -PI / 2., 0.0, 0.0),
            limits: (-169.5 * PI / 180., 169.5 * PI / 180.),
            speed_limit: 110. * PI / 180.,
        },
        Desc {
            // segment 2
            joint_type: JointType::RotZ,
            joint_tf: Frame::from_translation_euler(0.0, -0.2, 0.0, PI / 2., 0.0, 0.0),
            limits: (-119.5 * PI / 180., 119.5 * PI / 180.0),
            speed_limit: 110.0 * PI / 180.0,
        },
        Desc {
            // segment 3
            joint_type: JointType::RotZ,
            joint_tf: Frame::from_translation_euler(0.0, 0.0, 0.2, PI / 2.0, 0.0, 0.0),
            limits: (-169.5 * PI / 180.0, 169.5 * PI / 180.0),
            speed_limit: 128.0 * PI / 180.0,
        },
        Desc {
            //segment 4
            joint_type: JointType::RotZ,
            joint_tf: Frame::from_translation_euler(0.0, 0.2, 0.0, -PI / 2.0, 0.0, 0.0),
            limits: (-119.5 * PI / 180.0, 119.0 * PI / 180.0),
            speed_limit: 128.0 * PI / 180.0,
        },
        Desc {
            //segment 5
            joint_type: JointType::RotZ,
            joint_tf: Frame::from_translation_euler(0.0, 0.0, 0.19, -PI / 2., 0.0, 0.0),
            limits: (-169.5 * PI * 180.0, 169.5 * PI / 180.0),
            speed_limit: 204.0 * PI / 180.0,
        },
        Desc {
            //segment 6
            joint_type: JointType::RotZ,
            joint_tf: Frame::from_translation_euler(0.0, -0.078, 0.0, PI / 2.0, 0.0, 0.0),
            limits: (-119.5 * PI / 180.0, 119.5 * PI / 180.0),
            speed_limit: 184.0 * PI / 180.0,
        },
        Desc {
            //segment 7
            joint_type: JointType::RotZ,
            joint_tf: Frame::from_translation_euler(0.0, 0.0, 0.1, 0.0, 0.0, 0.0),
            limits: (-169.5 * PI / 180.0, 169.5 * PI / 180.0),
            speed_limit: 184.5 * PI / 180.0,
        },
    ]
}

fn create_arm() -> KinematicArm {
    let kuka_description = create_kuka_description();
    KinematicArm::build_from_description(kuka_description)
}

fn kinematic_arm_set_joint_states() {
    let kuka_description = create_kuka_description();
}

fn kinematic_arm_differential_move(mut testing_arm: KinematicArm) {
    let twist = Twist::new(0.1, 0.1, 0.1, 0.1, 0.1, 0.1);
    testing_arm.cartesian_diff_move(twist, 1.0 / 60.0);
}

fn kinematic_arm_set_joints(mut testing_arm: KinematicArm) {
    testing_arm.set_joint_states(vec![2.0, 2.0, 2.0, 2.0, 2.0, 2.0, 2.0]);
}

fn kinematic_arm_reachable_pose(mut testing_arm: KinematicArm) {
    let goal = Frame::from_translation_euler(0.5, 0.0, 0.5, 0.0, 1.0, 0.0);

    let result = testing_arm.cartesian_move(
        goal,               // Goal
        0.5,                // Speed
        150.0 * PI / 180.0, // Rot speed
        1.0 / 60.0,         // dt
        10.0,               // Timeout
        0.05,               // Tolerance
        5.0 * PI / 180.0,   // Rot Tolerance
    );
    assert!(result.is_ok());
}

fn kinematic_arm_unreachable_pose(mut testing_arm: KinematicArm) {
    let goal = Frame::from_translation_euler(3.0, 0.0, 3.0, 0.0, 0.0, 0.0);
    let result = testing_arm.cartesian_move(
        goal,
        0.5,
        150.0 * PI / 180.0,
        1.0 / 60.0,
        10.0,
        0.05,
        5.0 * PI / 180.0,
    );
    assert!(result.is_err());
}

fn kinematic_arm_random_pose(mut rng: ThreadRng, mut testing_arm: KinematicArm) {
    // Generate a random orientation
    let roll = rng.gen_range(-PI, PI);
    let pitch = rng.gen_range(-PI / 2.0, PI / 2.0);
    let yaw = rng.gen_range(-PI, PI);
    // Generate a random hollow sphere
    let r = rng.gen_range(0.3, 1.2);
    let theta = rng.gen_range(-PI, PI);
    let phi = rng.gen_range(-PI / 2.0, PI / 2.0);
    let x = r * phi.cos() * theta.cos();
    let y = r * phi.cos() * theta.sin();
    // Correct unreachable z positions
    let z_prev = r * phi.sin();
    let z = if z_prev < -0.6 { -z_prev } else { z_prev };
    let goal = Frame::from_translation_euler(x, y, z, roll, pitch, yaw);
    // Create the arm
    let _result = testing_arm.cartesian_move(
        goal,
        0.5,
        150.0 * PI / 180.0,
        1.0 / 60.0,
        10.0,
        0.05,
        5.0 * PI / 180.0,
    );
}

fn kinematic_arm_ik_one_iteration(mut testing_arm: KinematicArm) {
    let goal = Frame::from_translation_euler(0.5, 0.0, 0.5, 0.0, 1.0, 0.0);

    let result = testing_arm.cartesian_move(
        goal,               // Goal
        0.5,                // Speed
        150.0 * PI / 180.0, // Rot speed
        1.0 / 60.0,         // dt
        1.0 / 60.0,         // Timeout
        0.05,               // Tolerance
        5.0 * PI / 180.0,   // Rot Tolerance
    );
    assert!(result.is_err());
}

fn criterion_benchmark(c: &mut Criterion) {
    let rng = rand::thread_rng();
    let kuka_description = create_kuka_description();
    let mut arm = KinematicArm::build_from_description(kuka_description);
    // Bring the arm out of 0000000
    arm.set_joint_states(vec![1.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0]);
    // set lower IK parameters
    arm.set_ik_convergence_parameters(1e-200, 100);

    c.bench_function("create_arm", |b| b.iter(|| create_arm()));
    let mut arm1 = arm.clone(); // TODO: Hack!
    c.bench_function("reachable_pose", move |b| {
        b.iter(|| kinematic_arm_reachable_pose(arm1.clone()))
    });
    let mut arm2 = arm.clone(); // TODO: Hack! learn lifetimes!
    c.bench_function("unreachable_pose", move |b| {
        b.iter(|| kinematic_arm_unreachable_pose(arm2.clone()))
    });
    let mut arm3 = arm.clone(); // TODO: Hack! learn lifetimes!
    c.bench_function("random_pose", move |b| {
        b.iter(|| kinematic_arm_random_pose(rng, arm3.clone()))
    });
    let mut arm4 = arm.clone(); // TODO: Hack! learn lifetimes!
    c.bench_function("diff_move", move |b| {
        b.iter(|| kinematic_arm_differential_move(arm4.clone()))
    });
    let mut arm5 = arm.clone(); // TODO: Hack! learn lifetimes!
    c.bench_function("set_joints", move |b| {
        b.iter(|| kinematic_arm_set_joints(arm5.clone()))
    });
    let mut arm6 = arm.clone(); // TODO: Hack! learn lifetimes!
    c.bench_function("IK_one_iteration", move |b| {
        b.iter(|| kinematic_arm_ik_one_iteration(arm6.clone()))
    });
}

criterion_group! {
    name = benches;
    config = Criterion::default().sample_size(200);
    targets = criterion_benchmark
}

criterion_main!(benches);
