# ARCOS-KDL

## v0.3.3
- Upate nalgebra to 0.33.2, serde to 1.0.218, rand to 0.9.0, and criterion to 0.5.1

## v0.3.2
- Add support for Criterion benchmarks
- Add basic benchmarks for kinematic arms
- Fix a bug that prevented Kinematic arms to have unmovable joints in the middle
  of the kinematic chain
- Add the weight ranges to the kinematic arm. It is not required to set the
  "weight when close" every time the joint states are set, now that is handled
  internally.

## v0.3.1
- Fix a major bug with the non-differential controller for kinematic arms

## v0.3.0
- Make `SegmentDescription` components public.
- Fix svd test warnings.
- Enable `kinematicArm`s to return their description.
- Enable `Segment`s to return the transformation of their link.
- Fix cargo fmt warnings.
- Add a trait for handling different rotation representations.
- Allow user to have info about the kinematic arm: limits, speed limits, number
  of joints and number of segments without having to ask to the entire
  description.

## v0.2.4
- Update the features in the README to include positional inverse kinematics,
  which was achieved with the `KinematicArm`
- Implement `PartialEq` for `KinematicArm` and all its dependencies (chains,
  segments, and joints).

## v0.2.3
- Improve Kinematic arm module documentation
- Fix bug where a panic was raised when no SVD solutions where found. Now it is
  handled with a Result closure.

## v0.2.2
- Update the format file
- Add the kinematic arm module

## v0.2.1
- support for serde
- The inverse kinematics differential solver returns a Result closure instead of panicking when in finds non-singular solutions
- Formatting improvements (compatible with added rustfmt)

## v0.2.0

- Solvers are now structs
- frame module renamed to geometry
- New ways to build frames were added
- A Prelude was added
- Removed approx as dependency
- Changed rand to be just dev-dependency

## v0.1.0
- First release
