# ARCOS KDL

[![pipeline status](https://gitlab.com/arcoslab/arcos-kdl/badges/master/pipeline.svg)](https://gitlab.com/arcoslab/arcos-kdl/commits/master)
[![crates.io version badge](https://img.shields.io/crates/v/arcos-kdl.svg)](https://crates.io/crates/arcos-kdl)
[![Documentation](https://docs.rs/arcos-kdl/badge.svg)](https://docs.rs/crate/arcos-kdl)

Our implementation of a very simple Kinematic Dynamic Library. We were using orocos-KDL at
our laboratory, but found that it was better for our research interests to have our own library, so we created
arcos-kdl (as similar as possible to orocos-kdl).

## Features

- [x] Forward positional chain kinematics
- [x] Forward differential chain kinematics
- [x] Inverse positional chain kinematics
- [x] Inverse differential chain kinematics
- [ ] Forward chain dynamics
- [ ] Inverse chain dynamics
- [ ] Forward Tree kinematics

Right now arcos-kdl is capable of computing forward kinematics (positional and differential)
for arbitrary long kinematic chains. It is also capable of computing inverse differential kinematics
of arbitrary long chains.

For inverse kinematics we are using a algorithm called by orocos-kdl as weighted damped least squares.
The most important aspect of this algorithm is that one can define weight matrices at the task and joint
spaces. This weights indicates to the solver how much each (joint/axis) matters for the solution. So,
in the task space, the algorithm will priorize to reduce the error on those axes with higher weight. In the
joint space the algorithm will move less the joints with lower weight. This is very useful when the joint is
approaching a limit or a singularity; one can lower its weight, so that the algorithm uses more the joints
without problems.

For more information about this algorithm read the following papers:
  * A. Ben Israel & T.N.E. Greville. Generalized Inverses : Theory and Applications, second edition. Springer, 2003. ISBN 0-387-00293-6.

  * K. L. Doty, C. Melchiorri & C. Boniveto. A theory of generalized inverses applied to Robotics. The International Journal of Robotics Research, vol. 12, no. 1, pages 1-19, february 1993. 

ARCOS-KDL supports 

## Intalling

Execute:

``` bash
cargo install arcos-kdl
```

## Building

If you do not want to use the official package, you can build it yourself. Make sure you have installed cargo:

``` bash
cargo build --lib
```

## Testing

For executing the already implemented tests:

``` bash
cargo test
```

For benchmarks:

``` bash
cargo bench -- --noplot
```

## Documentation

The master documentation can be found [here](https://arcoslab.gitlab.io/arcos-kdl/arcos_kdl/index.html)
If you are looking for the stable API documentation, it can be found [here](https://docs.rs/crate/arcos-kdl/)

For generating the documentation yourself execute:

```bash
cargo doc
```

## Formatting

If you want to test your formatting do the following (take into consideration
that our continuous integration script will check the format and any
ill-formatted code will be prevented from merging master):

```bash
rustup component add rustfmt
cargo fmt --all -- --check
```

## License

Licensed under the [General Public License, version 3.0](https://www.gnu.org/licenses/gpl-3.0.html).

## Contributing

Please open an issue or merge request on GitLab to contibute. Code contributions submitted for
inclusion in the work by you, as defined in the GPLv3 license, shall be licensed as the above
without any additional terms or conditions. Thanks.
