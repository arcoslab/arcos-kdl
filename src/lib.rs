// Copyright (c) 2019 Autonomous Robots and Cognitive Systems Laboratory
// Author: Daniel Garcia-Vaglio <degv364@gmail.com>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

#![doc(
    html_logo_url = "https://assets.gitlab-static.net/uploads/-/system/project/avatar/12217395/arcos-kdl-logo.png?width=128",
    html_favicon_url = "https://assets.gitlab-static.net/uploads/-/system/project/avatar/12217395/arcos-kdl-logo.png?width=128"
)]
#![deny(missing_docs)]

/*!
# ARCOS-KDL

## ARCOS-Lab Kinematics and Dynamic Library.

arcos-kdl has been designed to be as similar as possible to
[orocos-kdl](http://www.orocos.org/wiki/orocos/kdl-wiki)

## Building

Execute:

```.bash
    cargo build --lib
```

## Testing

Execute:

```.bash
    cargo test
```

## Documentation

Execute:

```.bash
   cargo doc
```

## Examples:
### Forward velocity kinematics

Given the current state at each joint we want to calculate the
velocity at the end-effector.


```.rust
extern crate arcos_kdl;

use arcos_kdl::prelude::*;
fn main() {

    println!("Documentation!");
}
```
*/

/// Implementation of Kinematic chains
pub mod chains;
/// Forward kinematics differential solver
pub mod forward_diff_kinematics;
/// Forward kinematics solver
pub mod forward_kinematics;
/// Basic geometric constructs like Homogeneous transformations
pub mod geometry;
/// Inverse Kinematics differential solver
pub mod inverse_diff_kinematics;
/// Implementation of Hexadimensional kinematic Jacobians
pub mod jacobian;
/// Implementation of robotic Joints
pub mod joint;
/// Implementation of a kinematic robotic arm
pub mod kinematic_arm;
/// Implementation of kinematic chain segments
pub mod segment;
/// Special SVD computation algorithm
pub mod svd_eigen;

/// Re-exporting usefull stuff
pub mod prelude {
    pub use crate::chains::Chain;
    pub use crate::forward_diff_kinematics::ForwardDiffKinematicsSolver;
    pub use crate::forward_kinematics::ForwardKinematicsSolver;
    pub use crate::geometry::{
        add_delta, diff, invert_frame, new_ref_base, new_ref_frame, new_ref_point, EulerBuild,
        Frame, Introspection, Rotation, RotationRepresentations, Twist, Vector,
    };
    pub use crate::inverse_diff_kinematics::InverseDiffKinematicsSolver;
    pub use crate::jacobian::{Jacobian, JointOperations};
    pub use crate::joint::{Joint, JointType};
    pub use crate::kinematic_arm::{KinematicArm, SegmentDescription};
    pub use crate::segment::Segment;
}
